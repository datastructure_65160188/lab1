/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab1;

import java.util.Arrays;

/**
 *
 * @author black
 */
public class Lab1_2 {

    public static void main(String[] args) {
        int[] arr = {1, 0, 2, 3, 0, 4, 5, 0};
        duplicateZeros(arr);
        System.out.print("[ ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.print("]");
    }

    public static void duplicateZeros(int[] arr) {
        int l = arr.length; 
        int[] result = new int[l];
        int duplicateZeroIndex = 0;

        for (int num : arr) {
            if (num == 0) {
                result[duplicateZeroIndex++] = 0;
                if (duplicateZeroIndex < l) {
                    result[duplicateZeroIndex++] = 0;
                }
            } else {
                result[duplicateZeroIndex++] = num;
            }
            if (duplicateZeroIndex >= l) {
                break;
            }
        }
        System.arraycopy(result, 0, arr, 0, l);
    }

}
